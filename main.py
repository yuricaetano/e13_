def characters_filter(list_of_characters, key, value):
    character =  filter(lambda x: x[key] == value, list_of_characters)
    return character

def team_power(list_of_characters):
    power = 0
    power = sum(map( lambda x: x['power'], list_of_characters ))
    return power

from functools import reduce

def team_power_2(list_of_characters, advantage):
    result = team_power(list_of_characters)
    result += advantage
    return result