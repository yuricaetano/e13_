from main import characters_filter, team_power, team_power_2

def test_characters_filter():

    list_of_characters = [
    {
        "id": 1,
        "name": "huck",
        "intelligence": 9,
        "power": 7,
        "strength": 10,
        "agility": 8
    },
    {
        "id": 2,
        "name": "super man",
        "intelligence": 10,
        "power": 10,
        "strength": 10,
        "agility": 10
    }
]
    expected = [{
        "id": 2,
        "name": "super man",
        "intelligence": 10,
        "power": 10,
        "strength": 10,
        "agility": 10
    }]

    result = characters_filter(list_of_characters, 'agility', 10)  
    
    assert result == expected

def test_team_power():
    list_of_characters = [{
        "id": 1,
        "name": "huck",
        "intelligence": 9,
        "power": 7,
        "strength": 10,
        "agility": 8
    },
    {
        "id": 2,
        "name": "super man",
        "intelligence": 10,
        "power": 10,
        "strength": 10,
        "agility": 10
    }
    ]
    expected = 17

    result = team_power(list_of_characters)
    assert result == expected

def test_team_power_2():

    list_of_characters = [
    {
        "id": 1,
        "name": "huck",
        "intelligence": 9,
        "power": 7,
        "strength": 10,
        "agility": 8
    },
    {
        "id": 2,
        "name": "super man",
        "intelligence": 10,
        "power": 10,
        "strength": 10,
        "agility": 10
    }
    ]
    advantage = 3
    expected = 20
    
    result = team_power_2(list_of_characters, advantage)

